﻿namespace Winfred.Calculator.Tests
{
    using System.Linq;
    using Library;
    using NUnit.Framework;

    [TestFixture]
    public class CalculatorInputHandlerTests
    {
        private IInputHandler inputHandler;

        [SetUp]
        public void SetUp()
        {
            inputHandler = new CalculatorInputHandler();
        }

        [Test]
        public void Two_Plus_Two_Should_Equal_Four()
        {
            var item = inputHandler.Handle("=2+2").First();
            Assert.That(item.Header, Is.EqualTo("4"));
        }

        [Test]
        public void An_Empty_Expression_Should_Result_In_Zero()
        {
            var item = inputHandler.Handle("=").First();
            Assert.That(item.Header, Is.EqualTo("0"));
        }

        [Test]
        public void An_Expression_With_One_Operand_And_No_Operator_Should_Result_In_The_Operands_Value()
        {
            var item = inputHandler.Handle("=5").First();
            Assert.That(item.Header, Is.EqualTo("5"));
        }

        [Test]
        public void An_Expression_With_One_Operand_And_One_Operator_Should_Result_In_The_Operands_Value()
        {
            var item = inputHandler.Handle("=12*").First();
            Assert.That(item.Header, Is.EqualTo("12"));

            item = inputHandler.Handle("=5*").First();
            Assert.That(item.Header, Is.EqualTo("5"));
        }

        [Test]
        public void Multiplying_By_Zero_Should_Result_In_Zero()
        {
            var item = inputHandler.Handle("=5*0").First();
            Assert.That(item.Header, Is.EqualTo("0"));
        }

        [Test]
        public void Dividing_By_Zero_Should_Result_In_Zero()
        {
            var item = inputHandler.Handle("=5/0").First();
            Assert.That(item.Header, Is.EqualTo("0"));
        }

        [Test]
        public void Dividing_Thirty_By_Five_Should_Result_In_Six()
        {
            var item = inputHandler.Handle("=30/5").First();
            Assert.That(item.Header, Is.EqualTo("6"));
        }

        [Test]
        public void Subtracting_Ten_From_ThirtyOne_Should_Result_In_TwentyOne()
        {
            var item = inputHandler.Handle("=31-10").First();
            Assert.That(item.Header, Is.EqualTo("21"));
        }

        [Test]
        public void Multiplying_ThreeAndAHalf_By_Five_Should_Result_In_SeventeenAndAHalf()
        {
            var item = inputHandler.Handle("=3.5*5").First();
            Assert.That(item.Header, Is.EqualTo("17.5"));
        }
    }
}