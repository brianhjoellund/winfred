﻿namespace Winfred.Calculator.Tests
{
    using System;
    using NUnit.Framework;

    [TestFixture]
    class ParserTests
    {
        private Parser parser;

        [SetUp]
        public void SetUp()
        {
            parser = new Parser();
        }

        [Test]
        public void A_Single_Operand_Should_Result_In_An_Expression_With_A_Value_Equal_To_The_Operand()
        {
            var result = parser.Parse("2");

            Assert.That(result, Is.Not.Null);
            Assert.That(result, Is.InstanceOf<Expression>());
            Assert.That(result.Value, Is.EqualTo(2));
        }

        [Test]
        public void A_Single_Operand_And_An_Operator_Should_Result_In_An_Expression_With_A_Value_Equal_To_The_Operand()
        {
            var result = parser.Parse("2+");

            Assert.That(result, Is.Not.Null);
            Assert.That(result, Is.InstanceOf<Expression>());
            Assert.That(result.Value, Is.EqualTo(2));
        }

        [Test]
        public void Adding_Two_And_Two_Should_Result_In_A_BinaryExpression_With_A_Value_Of_Four()
        {
            var result = parser.Parse("2+2");

            Assert.That(result, Is.InstanceOf<BinaryExpression>());
            var binaryExpression = (BinaryExpression) result;
            Assert.That(binaryExpression.LeftHandSide, Is.InstanceOf<Expression>());
            Assert.That(binaryExpression.RightHandSide, Is.InstanceOf<Expression>());
            Assert.That(result.Value, Is.EqualTo(4));
        }

        [Test]
        public void Dividing_Four_By_Two_Should_Result_In_A_BinaryExpression_With_A_Value_Of_Two()
        {
            var result = parser.Parse("4/2");

            Assert.That(result, Is.InstanceOf<BinaryExpression>());
            var binaryExpression = (BinaryExpression)result;
            Assert.That(binaryExpression.LeftHandSide, Is.InstanceOf<Expression>());
            Assert.That(binaryExpression.RightHandSide, Is.InstanceOf<Expression>());
            Assert.That(result.Value, Is.EqualTo(2));
        }

        [Test]
        public void Multiplying_Two_By_Two_Should_Result_In_A_BinaryExpression_With_A_Value_Of_Four()
        {
            var result = parser.Parse("2*2");

            Assert.That(result, Is.InstanceOf<BinaryExpression>());
            Assert.That(result.Value, Is.EqualTo(4));
        }

        [Test]
        public void Multiplying_TwoAndAHalf_By_Two_Should_Result_In_A_BinaryExpression_With_A_Value_Of_Five()
        {
            var result = parser.Parse("2.5*2");

            Assert.That(result, Is.InstanceOf<BinaryExpression>());
            Assert.That(result.Value, Is.EqualTo(5));
        }

        [Test]
        public void Dividing_TwentyFive_By_TwoAndAHalf_Should_Result_In_A_BinaryExpression_With_A_Value_Of_Ten()
        {
            var result = parser.Parse("25/2.5");

            Assert.That(result, Is.InstanceOf<BinaryExpression>());
            Assert.That(result.Value, Is.EqualTo(10));
        }

        [Test]
        public void Subtracting_Five_From_Eleven_Should_Result_In_A_BinaryExpression_With_A_Value_Of_Six()
        {
            var result = parser.Parse("11-5");

            Assert.That(result, Is.InstanceOf<BinaryExpression>());
            Assert.That(result.Value, Is.EqualTo(6));
        }

        [Test]
        public void Subtracting_Four_From_ThirteenAndAHalf_Should_Result_In_A_BinaryExpression_With_A_Value_Of_NineAndAHalf()
        {
            var result = parser.Parse("13.5-4");

            Assert.That(result, Is.InstanceOf<BinaryExpression>());
            Assert.That(result.Value, Is.EqualTo(9.5));
        }

        [Test]
        public void Adding_FourAndAHalf_And_FiveAndAHalf_Should_Result_In_A_BinaryExpression_With_A_Value_Of_Ten()
        {
            var result = parser.Parse("4.5+5.5");

            Assert.That(result, Is.InstanceOf<BinaryExpression>());
            Assert.That(result.Value, Is.EqualTo(10));
        }

        [Test]
        public void Whitespace_In_The_Input_Should_Be_Ignored()
        {
            var result = parser.Parse("6      \t+\n6");

            Assert.That(result, Is.InstanceOf<BinaryExpression>());
            Assert.That(result.Value, Is.EqualTo(12));
        }

        [Test]
        public void Invalid_Tokens_In_The_Input_Should_Result_In_An_Expression_With_A_Value_Of_Zero()
        {
            var result = parser.Parse("6 + t");

            Assert.That(result, Is.InstanceOf<Expression>());
            Assert.That(result.Value, Is.EqualTo(0));
        }

        [Test]
        public void Using_PI_As_A_Constant_Will_Result_In_A_Value_Approximatly_That_Of_3DotOneFourOneFive()
        {
            var result = parser.Parse("PI");

            Assert.That(result, Is.InstanceOf<Expression>());
            Assert.That(result.Value, Is.EqualTo((decimal)Math.PI));
        }

        [Test]
        public void Using_TwoAndAHalf_As_Input_Should_Result_In_An_Expression_With_A_Value_Of_TwoAndAHalf()
        {
            var result = parser.Parse("2.5");

            Assert.That(result, Is.InstanceOf<Expression>());
            Assert.That(result.Value, Is.EqualTo(2.5m));
        }

        [Test]
        public void Four_To_The_Power_Of_Two_Should_Result_In_A_BinaryExpression_With_The_Value_Of_Sixteen()
        {
            var result = parser.Parse("4^2");

            Assert.That(result, Is.InstanceOf<BinaryExpression>());
            Assert.That(result.Value, Is.EqualTo(16m));
        }

        [Test]
        public void TwoAndAHalf_Raised_To_The_Power_Of_Two_Should_Result_In_A_BinaryExpression_With_The_Value_Of_SixPointTwoFive()
        {
            var result = parser.Parse("2.5^2");

            Assert.That(result, Is.InstanceOf<BinaryExpression>());
            Assert.That(result.Value, Is.EqualTo(6.25m));
        }

        [Test]
        public void An_Empty_Expression_Should_Result_In_An_Expression_With_The_Value_Of_Zero()
        {
            var result = parser.Parse("");

            Assert.That(result, Is.InstanceOf<Expression>());
            Assert.That(result.Value, Is.EqualTo(0));
        }

        [Test]
        public void Dividing_By_Zero_Should_Result_In_An_Expression_With_The_Value_Of_Zero()
        {
            var result = parser.Parse("2/0");

            Assert.That(result, Is.InstanceOf<Expression>());
            Assert.That(result.Value, Is.EqualTo(0));
        }
    }
}
