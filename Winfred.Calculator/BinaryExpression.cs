﻿namespace Winfred.Calculator
{
    using System;

    class BinaryExpression : Expression
    {
        public Expression LeftHandSide { get; set; }
        public Expression RightHandSide { get; set; }
        public Operator Operator { get; set; }

        public override decimal Value
        {
            get
            {
                decimal lhsValue = LeftHandSide.Value;
                decimal rhsValue = RightHandSide.Value;
                return PerformCalculation(lhsValue, rhsValue, Operator);
            }
        }

        private decimal PerformCalculation(decimal lhsValue, decimal rhsValue, Operator @operator)
        {
            switch (@operator)
            {
                case Operator.Addition:
                    return lhsValue + rhsValue;
                case Operator.Division:
                    if (decimal.Zero == lhsValue || decimal.Zero == rhsValue)
                        return decimal.Zero;

                    return lhsValue/rhsValue;
                case Operator.Multiplication:
                    return lhsValue*rhsValue;
                case Operator.Subtraction:
                    return lhsValue - rhsValue;
                case Operator.Power:
                    return new decimal(Math.Pow(Decimal.ToDouble(lhsValue), Decimal.ToDouble(rhsValue)));
            }

            return 0;
        }
    }
}