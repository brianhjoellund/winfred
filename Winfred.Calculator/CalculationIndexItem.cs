﻿namespace Winfred.Calculator
{
    using Library;
    using System;
    using System.Globalization;
    using System.Threading;
    using System.Windows;

    class CalculationIndexItem : IndexItem
    {
        public CalculationIndexItem()
            : base("Calculator", null)
        {
        }

        public decimal Result { get; set; }

        public override void Execute()
        {
            for (var i = 0; i < 3; ++i )
                try
                {
                    Clipboard.SetText(Result.ToString(CultureInfo.InvariantCulture));
                    return;
                }
                catch (Exception)
                {
                    Thread.Sleep(200);
                }

            throw new Exception("Could not copy result to clipboard");
        }

        protected override string GenerateId()
        {
            return "1";
        }
    }
}
