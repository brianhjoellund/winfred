﻿namespace Winfred.Calculator
{
    using Library;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using Properties;

    public class CalculatorInputHandler : IInputHandler
    {
        private byte[] cachedIcon;

        public HandlerCapabilities Capabilities { get { return HandlerCapabilities.None; } }

        public bool CanHandle(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
                return false;

            if (String.IsNullOrWhiteSpace(Settings.Default.StartToken))
                return true;

            return input.StartsWith(Settings.Default.StartToken);
        }

        public IEnumerable<IndexItem> Handle(string input)
        {
            var parser = new Parser();
            string expression = input.Substring(Settings.Default.StartToken.Length, input.Length - Settings.Default.StartToken.Length);

            return ToResultItem(parser.Parse(expression).Value, expression);
        }

        public IAction HandleTab(IndexItem item)
        {
            return null;
        }

        private IEnumerable<IndexItem> ToResultItem(decimal result, string expression)
        {
            return new IndexItem[]
                {
                    new CalculationIndexItem
                        {
                            Result = result,
                            Header = result.ToString("###,##0.##########", CultureInfo.CurrentUICulture),
                            SubHeader = expression,
                            Icon = GetCalculatorIcon()
                        }
                };
        }

        private byte[] GetCalculatorIcon()
        {
            if (cachedIcon != null)
                return cachedIcon;
            
            using (var stream = typeof (CalculatorInputHandler).Assembly.GetManifestResourceStream(typeof (CalculatorInputHandler), "calculator.png"))
            {
                if (stream == null)
                    return null;

                var buffer = new byte[stream.Length];
                stream.Read(buffer, 0, buffer.Length);
                
                cachedIcon = new byte[buffer.Length];
                Array.Copy(buffer, cachedIcon, buffer.Length);
                
                return buffer;
            }
        }
    }
}
