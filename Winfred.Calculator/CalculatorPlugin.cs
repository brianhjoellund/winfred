﻿namespace Winfred.Calculator
{
    using Library;

    public class CalculatorPlugin : IPlugin
    {
        public ISettings Settings { get { return new CalculatorSettings(); } }
        public int PreferredRank { get { return 1; } }
        public string Name { get { return "Winfred Calculator"; } }
        public IInputHandler InputHandler { get; private set; }

        public void Initialize()
        {
            InputHandler = new CalculatorInputHandler();
        }
    }
}
