﻿namespace Winfred.Calculator
{
    using System.Windows.Controls;
    using Library;
    using Properties;

    class CalculatorSettings : ISettings
    {
        [Setting("Start token", typeof(TextBox))]
        public string StartToken
        {
            get { return Settings.Default.StartToken; }
            set 
            { 
                Settings.Default.StartToken = value;
                Settings.Default.Save();
            }
        }
    }
}