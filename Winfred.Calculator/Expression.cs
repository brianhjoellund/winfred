﻿namespace Winfred.Calculator
{
    class Expression
    {
        protected Expression(){}

        public Expression(decimal value)
        {
            Value = value;
        }

        public virtual decimal Value { get; private set; }
    }
}