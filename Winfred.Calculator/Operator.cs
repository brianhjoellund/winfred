﻿namespace Winfred.Calculator
{
    enum Operator
    {
        Addition,
        Subtraction,
        Multiplication,
        Division,
        Power
    }
}