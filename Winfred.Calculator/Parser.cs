﻿namespace Winfred.Calculator
{
    using System;
    using System.Globalization;

    class Parser
    {
        public Expression Parse(string source)
        {
            try
            {
                var tokens = Tokenizer.GetTokens(source);
                var expression = ParseTokensToExpression(tokens);
                return expression;
            }
            catch
            {
                return new Expression(0);
            }
        }

        private Expression ParseTokensToExpression(string[] tokens)
        {
            for (int i = 0; i < tokens.Length; i++)
            {
                string lhs = tokens[i];
                string op = null;
                string rhs = null;

                if(!IsNumber(lhs))
                    throw new Exception("Found invalid token");

                if (tokens.Length > i + 1)
                {
                    i++;
                    op = tokens[i];
                    
                    if(!IsOperator(op))
                        throw new Exception("Found invalid token");

                    if (tokens.Length > i + 1)
                    {
                        i++;
                        rhs = tokens[i];

                        if(!IsNumber(rhs))
                            throw new Exception("Found invalid token");
                    }
                }

                if (op == null || rhs == null)
                    return new Expression(decimal.Parse(lhs, CultureInfo.InvariantCulture));

                return new BinaryExpression
                           {
                               LeftHandSide = new Expression(decimal.Parse(lhs, CultureInfo.InvariantCulture)),
                               RightHandSide = new Expression(decimal.Parse(rhs, CultureInfo.InvariantCulture)),
                               Operator = GetOperatorFromValue(op)
                           };
            }

            throw new Exception("No expression");
        }

        private Operator GetOperatorFromValue(string op)
        {
            switch (op)
            {
                case "+":
                    return Operator.Addition;
                case "-":
                    return Operator.Subtraction;
                case "*":
                    return Operator.Multiplication;
                case "^":
                    return Operator.Power;
                default:
                    return Operator.Division;
            }
        }

        private bool IsOperator(string op)
        {
            return op == "+" || op == "-" || op == "*" || op == "/" || op == "^";
        }

        private bool IsNumber(string token)
        {
            decimal tmp;
            return decimal.TryParse(token, out tmp);
        }
    }
}
