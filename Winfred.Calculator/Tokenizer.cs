﻿namespace Winfred.Calculator
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Text;

    static class Tokenizer
    {
        private static readonly Dictionary<string, decimal> validMathematicalTokens = new Dictionary<string, decimal>
                                                                                          {
                                                                                              {"PI", (decimal)Math.PI}
                                                                                          };
        public static string[] GetTokens(string source)
        {
            var tokens = new List<string>();

            for (int i = 0; i < source.Length; i++)
            {
                char c = source[i];

                if (char.IsNumber(c))
                {
                    tokens.Add(ParseNumber(source, ref i));
                }
                else if (IsOperator(c))
                {
                    tokens.Add(c.ToString(CultureInfo.InvariantCulture));
                }
                else if (char.IsLetter(c))
                {
                    string token = ParseLetterToken(source, ref i);

                    if(!validMathematicalTokens.ContainsKey(token))
                        throw new Exception();

                    tokens.Add(GetTokenValue(token));
                }
                else if (char.IsWhiteSpace(c))
                {
                }
                else
                {
                    throw new Exception("Invalid token found");
                }
            }

            return tokens.ToArray();
        }

        private static string GetTokenValue(string token)
        {
            return validMathematicalTokens[token].ToString(CultureInfo.InvariantCulture);
        }

        private static string ParseLetterToken(string source, ref int i)
        {
            var sb = new StringBuilder();

            for (; i < source.Length; ++i)
            {
                char c = source[i];

                if (!char.IsLetter(c))
                {
                    --i;
                    break;
                }

                sb.Append(c);
            }

            return sb.ToString();
        }

        private static bool IsOperator(char c)
        {
            return c == '+' || c == '-' || c == '*' || c == '/' || c == '^';
        }

        private static string ParseNumber(string source, ref int i)
        {
            bool foundDecimalPoint = false;
            string value = "";

            for (; i < source.Length; i++)
            {
                char c = source[i];

                if (!char.IsNumber(c))
                {
                    if (c != '.' || foundDecimalPoint)
                    {
                        i--;
                        break;
                    }

                    foundDecimalPoint = true;
                }

                value += c;
            }

            return value;
        }
    }
}