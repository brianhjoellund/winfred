﻿namespace Winfred.FileSystem.Tests
{
    using System;
    using System.Linq;
    using NUnit.Framework;

    [TestFixture]
    public class FileSystemInputHandlerTests
    {
        [Test]
        public void Result_Should_Contain_No_More_Than_13_Items()
        {
            var handler = new FileSystemInputHandler();
            var foo = handler.Handle("C:\\");

            Assert.That(foo.Count(), Is.AtMost(13));
        }

        [Test]
        public void Typing_Anything_After_A_Slash_Should_Count_As_A_Search()
        {
            var handler = new FileSystemInputHandler();
            var foo = handler.Handle("C:\\c");

            foreach (var indexItem in foo)
            {
                var dirname = indexItem.Header.Substring(indexItem.Header.LastIndexOf('\\') + 1);
                Assert.That(dirname.StartsWith("c", StringComparison.OrdinalIgnoreCase));
            }

            foo = handler.Handle("C:\\tools\\g");

            foreach (var indexItem in foo)
            {
                var dirname = indexItem.Header.Substring(indexItem.Header.LastIndexOf('\\') + 1);
                Assert.That(dirname.StartsWith("g", StringComparison.OrdinalIgnoreCase));
            }
        }

        [Test]
        public void Input_Ending_In_A_Slash_Should_List_All_Allowed_Subfolders()
        {
            var handler = new FileSystemInputHandler();
            var directories = handler.Handle("C:\\cygwin\\");

            Assert.That(directories.Count(), Is.Not.EqualTo(0));
        }
    }
}
