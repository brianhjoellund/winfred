﻿namespace Winfred.FileSystem
{
    using System.IO;
    using Library;

    internal class ExpandFilePathAction : IAction
    {
        private string Header { get; set; }
        
        public ExpandFilePathAction(string header)
        {
            Header = header;
        }

        public void Execute(IContext context)
        {
            context.QueryText = string.Format("{0}{1}", Header, Path.DirectorySeparatorChar);
        }
    }
}