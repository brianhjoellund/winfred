﻿namespace Winfred.FileSystem
{
    using Library;
    using System.Diagnostics;

    public class FileSystemIndexItem : IndexItem
    {
        public FileSystemIndexItem(string header, byte[] icon)
            : base(header, icon)
        {
        }

        public override void Execute()
        {
            Process.Start(Header);
        }

        protected override string GenerateId()
        {
            return Header;
        }
    }
}