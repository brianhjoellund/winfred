﻿namespace Winfred.FileSystem
{
    using Library;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;

    class FileSystemInputHandler : IInputHandler
    {
        private static readonly Regex fileSystemRegex = new Regex(@"^\w:\\");

        public HandlerCapabilities Capabilities { get { return HandlerCapabilities.HandlesTab; } }

        public bool CanHandle(string input)
        {
            return fileSystemRegex.IsMatch(input);
        }

        public IEnumerable<IndexItem> Handle(string input)
        {
            string path = input;
            byte[] folderIcon = GetFolderIcon();

            if (!path.EndsWith("\\"))
            {
                var directoryInfo = new DirectoryInfo(path.Substring(0, path.LastIndexOf('\\') + 1));

                return from directory in directoryInfo.EnumerateDirectories(path.Substring(path.LastIndexOf('\\') + 1) + "*").Take(CommonSettings.MaximumNumberOfRowsToReturn)
                       select new FileSystemIndexItem(directory.FullName, folderIcon);
            }

            var rootInfo = new DirectoryInfo(input);

            var result = from directory in rootInfo.EnumerateDirectories().Where(dir => (dir.Attributes & FileAttributes.Hidden) == 0).Take(CommonSettings.MaximumNumberOfRowsToReturn)
                         select new FileSystemIndexItem(directory.FullName, folderIcon);

            if(!result.Any())
            {
                path = path.TrimEnd('\\');

                var directoryInfo = new DirectoryInfo(path.Substring(0, path.LastIndexOf('\\') + 1));

                result = from directory in directoryInfo.EnumerateDirectories(path.Substring(path.LastIndexOf('\\') + 1) + "*").Take(CommonSettings.MaximumNumberOfRowsToReturn)
                         select new FileSystemIndexItem(directory.FullName, folderIcon);
            }

            return result;
        }

        public IAction HandleTab(IndexItem item)
        {
            return new ExpandFilePathAction(item.Header);
        }

        private byte[] GetFolderIcon()
        {
            using (Stream stream = typeof(FileSystemInputHandler).Assembly.GetManifestResourceStream(typeof(FileSystemInputHandler), "folder.png"))
            {
                byte[] buffer = new byte[stream.Length];
                stream.Read(buffer, 0, buffer.Length);
                return buffer;
            }
        }
    }
}