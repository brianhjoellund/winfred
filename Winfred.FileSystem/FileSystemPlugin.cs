﻿namespace Winfred.FileSystem
{
    using Library;

    public class FileSystemPlugin : IPlugin
    {
        public ISettings Settings { get { return new FileSystemSettings(); } }
        public int PreferredRank { get { return 1; } }
        public string Name { get { return "Directory browser"; } }

        public IInputHandler InputHandler { get; private set; }

        public void Initialize()
        {
            InputHandler = new FileSystemInputHandler();
        }
    }
}
