﻿namespace Winfred.Indexer.Tests
{
    using NUnit.Framework;

    [TestFixture]
    public class StartMenuIndexerTests
    {
        [Test]
        public void Starting_The_Indexer_Should_Throw_An_Error_If_Path_Not_Found()
        {
            var indexer = new StartMenuIndexer();
            indexer.Start();
        }
    }
}
