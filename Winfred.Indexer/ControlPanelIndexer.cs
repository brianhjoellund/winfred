﻿namespace Winfred.Indexer
{
    using System;
    using Library;
    using Microsoft.Win32;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;

    class ControlPanelIndexer : IContentIndexer
    {
        private const string DefaultValueName = "";
        private const string ExecutableExtension = ".exe";
        private const string ControlPanelKeyName = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\ControlPanel\NameSpace";
        private const string ClassIdKeyName = "CLSID";
        private const string LocalizedStringValueName = "LocalizedString";
        private const string ResourcePrefix = "@";
        private const string DefaultIconKeyName = "DefaultIcon";
        private const string OpenCommandKeyName = @"Shell\Open\Command";
        private const string SubHeader = "Control Panel item";

        public event ItemIndexedEventHandler ItemIndexed;
        public IEnumerable<IndexItem> IndexItems { get; private set; }
        
        public void Start()
        {
            var controlPanelKey = Registry.LocalMachine.OpenSubKey(ControlPanelKeyName);

            if(controlPanelKey == null)
                return;

            var controlPanelClassIds = controlPanelKey.GetSubKeyNames();
            var systemClassIds = Registry.ClassesRoot.OpenSubKey(ClassIdKeyName);

            if(systemClassIds == null)
                return;

            var items = new List<SearchIndexItem>();

            foreach (string classId in controlPanelClassIds)
            {
                var controlPanelClassId = systemClassIds.OpenSubKey(classId);
                
                if(controlPanelClassId == null)
                    continue;

                string localizedString = (string) controlPanelClassId.GetValue(LocalizedStringValueName);
                string displayName;

                if (localizedString == null)
                {
                    displayName = controlPanelClassId.GetValue(DefaultValueName).ToString();
                }
                else
                {
                    displayName = localizedString;

                    if (localizedString.StartsWith(ResourcePrefix))
                        displayName = FileResourceExtractor.LoadResourceString(localizedString.Substring(ResourcePrefix.Length));
                }

                var iconKey = controlPanelClassId.OpenSubKey(DefaultIconKeyName);

                Icon icon = null;
                
                if(iconKey != null)
                    icon = FileResourceExtractor.LoadResourceIcon((string)iconKey.GetValue(DefaultValueName));

                string path;

                RegistryKey shellCommand = controlPanelClassId.OpenSubKey(OpenCommandKeyName);

                if (shellCommand != null)
                {
                    path = shellCommand.GetValue(DefaultValueName).ToString();

                    if(!path.TrimEnd().EndsWith(ExecutableExtension))
                    {
                        string executable = path.Substring(0, path.IndexOf(ExecutableExtension, StringComparison.OrdinalIgnoreCase) + 4);
                        path = path.Substring(path.IndexOf(ExecutableExtension, StringComparison.OrdinalIgnoreCase) + 5);
                        items.Add(new SearchIndexItem(displayName, executable, null, path, IconExtractor.GetIconAsByteArray(icon)) { SubHeader = SubHeader });
                    }
                    else
                    {
                        items.Add(new SearchIndexItem(displayName, path, null, IconExtractor.GetIconAsByteArray(icon)) { SubHeader = SubHeader});
                    }
                }
                else
                {
                    var appName = controlPanelClassId.GetValue("System.ApplicationName") as string;

                    if (String.IsNullOrWhiteSpace(appName))
                    {
                        path = Path.GetFileName(localizedString.Substring(0, localizedString.LastIndexOf(',')));

                        items.Add(new SearchIndexItem(displayName, "control", null, path,
                                                      IconExtractor.GetIconAsByteArray(icon)) {SubHeader = SubHeader});
                    }
                    else
                    {
                        items.Add(new SearchIndexItem(displayName, "control", null, String.Format("/name {0}", appName), IconExtractor.GetIconAsByteArray(icon)) { SubHeader = SubHeader });
                    }
                }
            }

            IndexItems = items;
        }

        public void Stop()
        {
        }
    }
}
