﻿namespace Winfred.Indexer
{
    using System;
    using System.Diagnostics;
    using System.Drawing;
    using System.Runtime.InteropServices;
    using System.Text;

    static class FileResourceExtractor
    {
        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern IntPtr LoadLibrary(string filename);

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern void FreeLibrary(IntPtr handle);

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        private static extern int LoadString(IntPtr handle, uint resourceId, StringBuilder buffer, int bufferLength);
        
        [DllImport("user32.dll")]
        private static extern IntPtr LoadIcon(IntPtr hInstance, string lpIconName);

        [DllImport("user32.dll")]
        private static extern IntPtr LoadIcon(IntPtr hInstance, IntPtr lpIconName);
        
        public static string LoadResourceString(string resourceDescriptor)
        {
            if (!resourceDescriptor.Contains(","))
                return GetNameFromVersionTable(resourceDescriptor);
            string dllName = resourceDescriptor.Substring(0, resourceDescriptor.LastIndexOf(','));
            string resource = resourceDescriptor.Substring(resourceDescriptor.LastIndexOf(',') + 1);

            if (resource.Contains("#"))
                resource = resource.Substring(0, resource.IndexOf('#'));

            uint resourceId = (uint)Math.Abs(Int32.Parse(resource));

            IntPtr handle = IntPtr.Zero;

            try
            {
                handle = LoadLibrary(dllName);
                var buffer = new StringBuilder(260);
                LoadString(handle, resourceId, buffer, buffer.Capacity + 1);
                return buffer.ToString();
            }
            finally
            {
                FreeLibrary(handle);
            }
        }

        private static string GetNameFromVersionTable(string fileName)
        {
            var fileInfo = FileVersionInfo.GetVersionInfo(fileName);
            return fileInfo.ProductName;
        }

        public static Icon LoadResourceIcon(string resourceDescriptor)
        {
            if (!resourceDescriptor.Contains(","))
            {
                FileIcon fileIcon = new FileIcon(resourceDescriptor);
                return fileIcon.ShellIcon;
            }

            string dllName = resourceDescriptor.Substring(0, resourceDescriptor.LastIndexOf(','));
            string resource = resourceDescriptor.Substring(resourceDescriptor.LastIndexOf(',') + 1);

            if (resource.Contains("#"))
                resource = resource.Substring(0, resource.IndexOf('#'));

            IntPtr resourceId = new IntPtr(Math.Abs(Int32.Parse(resource)));

            IntPtr handle = IntPtr.Zero;

            try
            {
                handle = LoadLibrary(dllName);
                IntPtr iconhAndle = LoadIcon(handle, resourceId);

                if (iconhAndle == IntPtr.Zero)
                    return null;

                return Icon.FromHandle(iconhAndle);
            }
            finally
            {
                FreeLibrary(handle);
            }
        }
    }
}
