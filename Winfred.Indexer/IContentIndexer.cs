﻿namespace Winfred.Indexer
{
    using Library;
    using System.Collections.Generic;

    public interface IContentIndexer
    {
        event ItemIndexedEventHandler ItemIndexed;
        IEnumerable<IndexItem> IndexItems { get; }
        void Start();
        void Stop();
    }
}
