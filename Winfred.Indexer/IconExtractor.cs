﻿namespace Winfred.Indexer
{
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.IO;

    static class IconExtractor
    {
        public static byte[] GetFileIconAsByteArray(string filename)
        {
            try
            {
                using (var link = new ShellLink(filename))
                {
                    var fileIcon = link.LargeIcon ?? link.SmallIcon ?? GetFileIconFromPath(link.Target);

                    return GetIconAsByteArray(fileIcon);
                }
            }
            catch
            {
                return null;
            }
        }

        public static byte[] GetIconAsByteArray(Icon icon)
        {
            if (icon == null)
                return null;

            using (var stream = new MemoryStream())
            {
                icon.ToBitmap().Save(stream, ImageFormat.Png);
                return stream.ToArray();
            }
        }

        private static Icon GetFileIconFromPath(string path)
        {
            var fileIcon = new FileIcon(path,
                                        FileIcon.SHGetFileInfoConstants.SHGFI_ICON |
                                        FileIcon.SHGetFileInfoConstants.SHGFI_LARGEICON);
            return fileIcon.ShellIcon;
        }

    }
}
