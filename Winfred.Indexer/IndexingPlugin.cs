﻿namespace Winfred.Indexer
{
    using Library;
    using System;

    public class IndexingPlugin : IPlugin
    {
        public ISettings Settings { get { return new IndexingSettings(); } }
        public int PreferredRank { get { return Int32.MaxValue; } }
        public string Name { get { return "Start menu and control panel indexer"; } }

        public IInputHandler InputHandler { get; private set; }

        public void Initialize()
        {
            InputHandler = new SearchInputHandler();
        }
    }
}
