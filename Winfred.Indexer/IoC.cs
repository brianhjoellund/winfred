﻿namespace Winfred.Indexer
{
    using Ninject;
    using Ninject.Modules;

    public static class IoC
    {
        private class Module : NinjectModule
        {
            public override void Load()
            {
                Bind<IContentIndexer>().To<StartMenuIndexer>();
            }
        }

        private static readonly IKernel Kernel = new StandardKernel(new Module());
        
        public static T Resolve<T>()
        {
            return Kernel.Get<T>();
        }
    }
}
