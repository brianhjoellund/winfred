﻿namespace Winfred.Indexer
{
    public delegate void ItemIndexedEventHandler(object sender, ItemIndexedEventHandlerArgs args);
}