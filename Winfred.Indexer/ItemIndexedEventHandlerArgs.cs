﻿namespace Winfred.Indexer
{
    using Library;
    using System;

    public class ItemIndexedEventHandlerArgs : EventArgs
    {
        private readonly IndexItem item;

        public ItemIndexedEventHandlerArgs(IndexItem item)
        {
            this.item = item;
        }

        public IndexItem Item
        {
            get { return item; }
        }
    }
}