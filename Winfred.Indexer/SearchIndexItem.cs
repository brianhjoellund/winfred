﻿namespace Winfred.Indexer
{
    using Library;
    using System;
    using System.Diagnostics;
    using System.Security.Cryptography;
    using System.Text;

    public class SearchIndexItem : IndexItem
    {
        public SearchIndexItem()
            : this("", "", "", null) {}

        public SearchIndexItem(string header, string path, string fileName, byte[] icon)
            : base(header, icon)
        {
            Path = path;
            SubHeader = path;
            FileName = fileName;
        }

        public SearchIndexItem(string header, string path, string fileName, string arguments, byte[] icon)
            : this(header, path, fileName, icon)
        {
            Arguments = arguments;
        }

        [Indexing(Store = true, Analyze = Analyze.No)]
        public string Path { get; set; }

        [Indexing(Store = true, Analyze = Analyze.No)]
        public string Arguments { get; set; }

        [Indexing(Store = true, Analyze = Analyze.Yes)]
        public string FileName { get; set; }

        public override void Execute()
        {
            Process.Start(Path, Arguments);

            SearchProvider.Current.Boost(Id);
        }

        protected override string GenerateId()
        {
            var bytes = Encoding.Default.GetBytes(String.Format("{0} {1} {2}", Path, Arguments, FileName).Trim());
            byte[] hash;
            
            using (var sha1 = SHA1.Create())
                hash = sha1.ComputeHash(bytes);

            return BitConverter.ToString(hash).Replace("-", "").ToLowerInvariant();
        }
    }
}
