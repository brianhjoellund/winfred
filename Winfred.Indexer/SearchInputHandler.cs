﻿namespace Winfred.Indexer
{
    using Library;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class SearchInputHandler : IInputHandler
    {
        private readonly List<IContentIndexer> indexers;

        public SearchInputHandler()
        {
            indexers = new List<IContentIndexer>
                {
                    new StartMenuIndexer(),
                    new ControlPanelIndexer()
                };

            IndexSearchLocations();
        }

        private void IndexSearchLocations()
        {
            var indexItems = new List<SearchIndexItem>();

            foreach (var indexer in indexers)
            {
                indexer.Start();

                IEnumerable<SearchIndexItem> searchItems = indexer.IndexItems.OfType<SearchIndexItem>().ToArray();
                SearchProvider.Current.Index(searchItems);
                indexItems.AddRange(searchItems);
            }
            SearchProvider.Current.RemoveFromIndexWhereNotIn(indexItems);
        }

        public HandlerCapabilities Capabilities { get { return HandlerCapabilities.None; } }

        public bool CanHandle(string input)
        {
            return !String.IsNullOrWhiteSpace(input);
        }

        public IEnumerable<IndexItem> Handle(string input)
        {
            return SearchProvider.Current.Search<SearchIndexItem>(input);
        }

        public IAction HandleTab(IndexItem item)
        {
            return null;
        }
    }
}