namespace Winfred.Indexer
{
    using Library;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    class StartMenuIndexer : IContentIndexer
    {
        private readonly List<FileSystemWatcher> watchers = new List<FileSystemWatcher>();

        public event ItemIndexedEventHandler ItemIndexed;

        public IEnumerable<IndexItem> IndexItems { get; private set; }
        
        private DirectoryInfo UserStartMenuDirectory
        {
            get
            {
                return GetSpecialFolderDirectoryInfo(Environment.SpecialFolder.StartMenu);
            }
        }

        private DirectoryInfo GetSpecialFolderDirectoryInfo(Environment.SpecialFolder specialFolder)
        {
            var path = Environment.GetFolderPath(specialFolder);
            return new DirectoryInfo(path);
        }

        private DirectoryInfo CommonStartMenuDirectory
        {
            get { return GetSpecialFolderDirectoryInfo(Environment.SpecialFolder.CommonStartMenu); }
        }

        public void Start()
        {
            InitialIndexing();

            SetupWatchers();
        }

        private void InitialIndexing()
        {
            var startMenu = UserStartMenuDirectory;
            var programsDirectory = startMenu.GetDirectories().First();

            IndexItems = IndexDirectory(programsDirectory)
                .AsParallel()
                .Concat(IndexDirectory(CommonStartMenuDirectory).AsParallel())
                .OrderBy(i => i.Header);
        }

        private static IEnumerable<SearchIndexItem> IndexDirectory(DirectoryInfo directoryInfo)
        {
            var items = directoryInfo
                .EnumerateFiles()
                .Where(fi => fi.Extension == ".lnk")
                .Select(MapFileInfoToIndexItem)
                .Where(item => item != null);

            return items
                .Concat(directoryInfo
                            .EnumerateDirectories()
                            .SelectMany(IndexDirectory))
                .ToList();

        }

        private static SearchIndexItem MapFileInfoToIndexItem(FileInfo arg)
        {
            try
            {
                var link = new ShellLink(arg.FullName);
                string fileName = Path.GetFileName(link.Target);
                return new SearchIndexItem(Path.GetFileNameWithoutExtension(arg.Name), arg.FullName, fileName,
                                           IconExtractor.GetFileIconAsByteArray(arg.FullName));
            }
            catch
            {
                return null;
            }
        }

        private void SetupWatchers()
        {
            WatchDirectory(UserStartMenuDirectory.FullName);
            WatchDirectory(CommonStartMenuDirectory.FullName);
        }

        private void WatchDirectory(string path)
        {
            var watcher = new FileSystemWatcher(path, "*.lnk")
                {
                    IncludeSubdirectories = true,
                    NotifyFilter = NotifyFilters.FileName
                };
            watcher.Created += LinkCreated;
            watcher.Deleted += LinkDeleted;
            watcher.Renamed += LinkRenamed;

            watcher.EnableRaisingEvents = true;

            watchers.Add(watcher);
        }

        private void LinkCreated(object sender, FileSystemEventArgs e)
        {
            var indexItem = MapFileInfoToIndexItem(new FileInfo(e.FullPath));
            SearchProvider.Current.Index(indexItem);
        }

        private void LinkDeleted(object sender, FileSystemEventArgs e)
        {
            var indexItem = MapFileInfoToIndexItem(new FileInfo(e.FullPath));
            SearchProvider.Current.RemoveFromIndex(indexItem);
        }

        private void LinkRenamed(object sender, RenamedEventArgs e)
        {
            var oldItem = MapFileInfoToIndexItem(new FileInfo(e.OldFullPath));
            var newItem = MapFileInfoToIndexItem(new FileInfo(e.FullPath));
            SearchProvider.Current.MoveItem(oldItem, newItem);
        }

        public void Stop()
        {
            foreach (var fileSystemWatcher in watchers)
                fileSystemWatcher.Dispose();
        }
    }
}