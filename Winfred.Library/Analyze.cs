﻿namespace Winfred.Library
{
    public enum Analyze
    {
        No,
        Yes,
        NotAnalyzed,
        NotAnalyzedNoNorms,
        AnalyzedNoNorms
    }
}