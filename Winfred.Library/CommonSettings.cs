﻿namespace Winfred.Library
{
    using Properties;

    public static class CommonSettings
    {
        public static int MaximumNumberOfRowsToReturn
        {
            get { return Settings.Default.MaximumNumberOfRowsToReturn; }
        }
    }
}
