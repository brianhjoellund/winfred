﻿namespace Winfred.Library
{
    using Lucene.Net.Index;
    using Lucene.Net.Search.Function;
    using System;
    using System.Globalization;

    class CustomSearchScoreProvider : CustomScoreProvider
    {
        public CustomSearchScoreProvider(IndexReader reader) : base(reader)
        {
        }

        public override float CustomScore(int doc, float subQueryScore, float valSrcScore)
        {
            float score;

            if (Single.TryParse(reader.Document(doc).GetField("score").StringValue, NumberStyles.Float, CultureInfo.InvariantCulture, out score))
                return score * subQueryScore;
            
            return 1 * subQueryScore;
        }
    }
}