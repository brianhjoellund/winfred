﻿namespace Winfred.Library
{
    using Lucene.Net.Index;
    using Lucene.Net.Search;
    using Lucene.Net.Search.Function;

    class CustomSearchScoreQuery : CustomScoreQuery
    {
        public CustomSearchScoreQuery(Query subQuery) : base(subQuery)
        {
        }

        public CustomSearchScoreQuery(Query subQuery, ValueSourceQuery valSrcQuery) : base(subQuery, valSrcQuery)
        {
        }

        public CustomSearchScoreQuery(Query subQuery, params ValueSourceQuery[] valSrcQueries) : base(subQuery, valSrcQueries)
        {
        }

        protected override CustomScoreProvider GetCustomScoreProvider(IndexReader reader)
        {
            return new CustomSearchScoreProvider(reader);
        }
    }
}