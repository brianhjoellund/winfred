﻿namespace Winfred.Library
{
    using Lucene.Net.Documents;
    using Lucene.Net.Index;
    using System;
    using System.Linq;

    class GenericIndexDefinition<TType> where TType : class
    {
        public Document Convert(TType entity)
        {
            var properties = typeof(TType).GetProperties();

            var document = new Document();

            foreach (var property in properties)
            {
                var value = property.GetValue(entity, null);
                if(value == null)
                    continue;

                IndexingAttribute attribute = property.GetCustomAttributes(typeof (IndexingAttribute), true).Cast<IndexingAttribute>().FirstOrDefault();

                if(attribute == null)
                    continue;

                string name = property.Name;

                if (!String.IsNullOrWhiteSpace(attribute.Name))
                    name = attribute.Name;

                if(property.PropertyType != typeof(byte[]))
                    document.Add(new Field(name, value.ToString(), GetStoreValue(attribute.Store), GetAnalyzedValue(attribute.Analyze)));
                else
                    document.Add(new Field(name, value as byte[], GetStoreValue(attribute.Store)));
            }

            document.Add(new Field("typeName", typeof(TType).FullName, Field.Store.YES, Field.Index.NOT_ANALYZED));
            document.Add(new NumericField("score", Field.Store.YES, false).SetFloatValue(1));

            return document;
        }

        private static Field.Index GetAnalyzedValue(Analyze analyze)
        {
            return (Field.Index)analyze;
        }

        private static Field.Store GetStoreValue(bool store)
        {
            if (store)
                return Field.Store.YES;

            return Field.Store.NO;
        }

        public Term GetIndex(TType entity)
        {
            var properties = typeof (TType).GetProperties();

            foreach (var property in properties)
            {
                var attribute = property.GetCustomAttributes(typeof(IndexingAttribute), true).Cast<IndexingAttribute>().FirstOrDefault();

                if(attribute == null)
                    continue;

                if (attribute.IsId)
                {
                    string name = property.Name;

                    if (!String.IsNullOrWhiteSpace(attribute.Name))
                        name = attribute.Name;

                    return new Term(name, property.GetValue(entity, null).ToString());
                }
            }

            throw new Exception("No ID property found for type " + typeof(TType).FullName);
        }
    }
}