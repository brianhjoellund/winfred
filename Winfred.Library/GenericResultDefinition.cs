﻿namespace Winfred.Library
{
    using Lucene.Net.Documents;
    using System;
    using System.Linq;

    class GenericResultDefinition<TType>
    {
        public TType Convert(Document document)
        {
            var type = typeof (TType);

            if(document.Get("typeName") != type.FullName)
                throw new Exception("Cannot convert document - Incompatible type");

            var entity = Activator.CreateInstance<TType>();

            foreach (var property in type.GetProperties())
            {
                var attribute = property.GetCustomAttributes(typeof (IndexingAttribute), true).Cast<IndexingAttribute>().FirstOrDefault();
                
                if(attribute == null || attribute.IsId)
                    continue;

                string name = property.Name;

                if (!String.IsNullOrWhiteSpace(attribute.Name))
                    name = attribute.Name;

                Field field = document.GetField(name);

                if(field == null)
                    continue;

                if (!field.IsBinary)
                {
                    property.SetValue(entity, field.StringValue, null);
                }
                else
                {
                    byte[] array = new byte[field.BinaryLength];
                    Array.Copy(field.GetBinaryValue(), field.BinaryOffset, array, 0, field.BinaryLength);
                    property.SetValue(entity, array, null);
                }
            }

            return entity;
        }
    }
}