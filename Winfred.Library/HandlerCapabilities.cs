﻿namespace Winfred.Library
{
    using System;

    [Flags]
    public enum HandlerCapabilities
    {
        None,
        HandlesTab
    }
}