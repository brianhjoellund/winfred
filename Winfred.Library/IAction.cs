﻿namespace Winfred.Library
{
    public interface IAction
    {
        void Execute(IContext context);
    }
}