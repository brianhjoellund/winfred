﻿namespace Winfred.Library
{
    public interface IContext
    {
        string QueryText { get; set; }
    }
}