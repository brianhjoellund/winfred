﻿namespace Winfred.Library
{
    using System;

    public interface IInitializeControl
    {
        void Initialize(object value);
        event EventHandler<ValueChangedEventArgs> ValueChanged;
    }
}
