﻿namespace Winfred.Library
{
    using System.Collections.Generic;

    public interface IInputHandler
    {
        HandlerCapabilities Capabilities { get; }
        bool CanHandle(string input);
        IEnumerable<IndexItem> Handle(string input);
        IAction HandleTab(IndexItem item);
    }
}
