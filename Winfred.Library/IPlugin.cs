﻿namespace Winfred.Library
{
    public interface IPlugin
    {
        string Name { get; }
        int PreferredRank { get; }
        ISettings Settings { get; }
        IInputHandler InputHandler { get; }
        void Initialize();
    }
}
