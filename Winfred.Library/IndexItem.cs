﻿namespace Winfred.Library
{
    public abstract class IndexItem
    {
        private string id;

        protected IndexItem(string header, byte[] icon)
        {
            Header = header;
            Icon = icon;
        }

        [Indexing(Store = true, Analyze = Analyze.Yes)]
        public string Header { get; set; }

        [Indexing(Store = true, Analyze = Analyze.No)]
        public byte[] Icon { get; set; }

        [Indexing(Store = true, Analyze = Analyze.No)]
        public string SubHeader { get; set; }

        [Indexing(Store = true, Analyze = Analyze.NotAnalyzed, IsId = true)]
        public string Id
        {
            get
            {
                if(id == null)
                    id = GenerateId();

                return id;
            }
        }

        public abstract void Execute();

        protected abstract string GenerateId();

        protected void InvalidateId()
        {
            id = null;
        }
    }
}
