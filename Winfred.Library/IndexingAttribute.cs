﻿namespace Winfred.Library
{
    using System;

    public class IndexingAttribute : Attribute
    {
        public string Name { get; set; }

        public bool Store { get; set; }

        public Analyze Analyze { get; set; }

        public bool IsId { get; set; }
    }
}