﻿namespace Winfred.Library
{
    using Lucene.Net.Analysis.Standard;
    using Lucene.Net.Documents;
    using Lucene.Net.Index;
    using Lucene.Net.QueryParsers;
    using Lucene.Net.Search;
    using Lucene.Net.Search.Function;
    using Lucene.Net.Store;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using Properties;
    using Version = Lucene.Net.Util.Version;

    public class SearchProvider
    {
        private static SearchProvider currentProvider;
        private const string IndexDirectoryName = "index";

        private SearchProvider()
        {}

        private string IndexDirectoryPath
        {
            get
            {
                string dirname = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Winfred", IndexDirectoryName);
                var directory = new DirectoryInfo(dirname);

                if(!directory.Exists)
                    directory.Create();

                return directory.FullName;
            }
        }

        public static SearchProvider Current
        {
            get
            {
                if (currentProvider == null)
                    currentProvider = new SearchProvider();

                return currentProvider;
            }
        }

        public void RemoveFromIndexWhereNotIn<TType>(IEnumerable<TType> entities) where TType: IndexItem
        {
            var query = new BooleanQuery(true)
                {
                    {new ConstantScoreQuery(new QueryWrapperFilter(new TermQuery(new Term("typeName", typeof (TType).FullName)))), Occur.MUST},
                    {GetIdQuery(entities), Occur.MUST_NOT}
                };

            using (var writer = GetIndexWriter())
                writer.DeleteDocuments(query);
        }

        private BooleanQuery GetIdQuery<TType>(IEnumerable<TType> entities) where TType: IndexItem
        {
            var clause = new BooleanQuery();

            foreach (var entity in entities)
                clause.Add(new TermQuery(new Term("Id", entity.Id)), Occur.SHOULD);

            return clause;
        }

        public void RemoveFromIndex<TType>(TType entity) where TType : IndexItem
        {
            if(!DocumentExists(entity))
                return;

            using (var writer = GetIndexWriter())
            {
                writer.DeleteDocuments(new Term("Id", entity.Id));
                writer.Commit();
            }
        }

        private IndexWriter GetIndexWriter(bool createIndexIfNotExists = false)
        {
            return new IndexWriter(FSDirectory.Open(new DirectoryInfo(IndexDirectoryPath)), new StandardAnalyzer(Version.LUCENE_30), createIndexIfNotExists, new IndexWriter.MaxFieldLength(260));
        }

        public void Index<TType>(TType entity) where TType : IndexItem
        {
            EnsureIndex();

            if(DocumentExists(entity))
                return;

            using (var writer = GetIndexWriter())
            {
                writer.AddDocument(new GenericIndexDefinition<TType>().Convert(entity));
                writer.Commit();
            }
        }

        private void EnsureIndex()
        {
            var indexDirectoryInfo = new DirectoryInfo(IndexDirectoryPath);
            var directory = FSDirectory.Open(indexDirectoryInfo);
            var checkIndex = new CheckIndex(directory);
            var status = checkIndex.CheckIndex_Renamed_Method();

            if(!indexDirectoryInfo.Exists || status.missingSegments)
                using (var writer = GetIndexWriter(createIndexIfNotExists:true))
                    writer.Commit();
        }

        private bool DocumentExists<TType>(TType entity) where TType: IndexItem
        {
            var doc = GetDocumentById(entity.Id);

            return doc != null;
        }

        private Document GetDocumentById(string id)
        {
            var query = new TermQuery(new Term("Id", id));

            using (var searcher = new IndexSearcher(FSDirectory.Open(new DirectoryInfo(IndexDirectoryPath))))
            {
                var result = searcher.Search(query, 1);

                if(result.TotalHits > 0)
                    return searcher.Doc(result.ScoreDocs[0].Doc);
            }

            return null;
        }

        public void Index<TType>(IEnumerable<TType> entities) where TType: IndexItem
        {
            EnsureIndex();

            using (var writer = GetIndexWriter())
            {
                var indexDefinition = new GenericIndexDefinition<TType>();

                foreach (var entity in entities)
                    if(!DocumentExists(entity))
                        writer.AddDocument(indexDefinition.Convert(entity));

                writer.Commit();
            }
        }

        public IEnumerable<TType> Search<TType>(string text) where TType: IndexItem
        {
            using (var searcher = new IndexSearcher(FSDirectory.Open(new DirectoryInfo(IndexDirectoryPath))))
            {
                var result = searcher.Search(GetQuery<TType>(text), Settings.Default.MaximumNumberOfRowsToReturn);

                var resultDefinition = new GenericResultDefinition<TType>();
                return result.ScoreDocs.Select(doc => resultDefinition.Convert(searcher.Doc(doc.Doc))).ToArray();
            }
        }

        private static Query GetQuery<TType>(string text)
        {
            var tokens = text.Split(new[] {'\t', ' '}, StringSplitOptions.RemoveEmptyEntries).Select(s => s.TrimStart('*'));
            var fields = GetFieldsForType<TType>();

            var newQuery = new BooleanQuery();

            foreach (string token in tokens)
            {
                var escapedToken = QueryParser.Escape(token);

                var fieldQuery = new BooleanQuery();
                
                foreach (string field in fields)
                    fieldQuery.Add(new PrefixQuery(new Term(field, escapedToken)), Occur.SHOULD);
                
                newQuery.Add(fieldQuery, Occur.MUST);
            }

            return new CustomSearchScoreQuery(newQuery, new FieldScoreQuery("score", FieldScoreQuery.Type.FLOAT));
        }

        private static string[] GetFieldsForType<T>()
        {
            var fields = new List<string>();
            var type = typeof (T);

            foreach (var property in type.GetProperties())
            {
                var attribute =
                    property.GetCustomAttributes(typeof (IndexingAttribute), true)
                            .Cast<IndexingAttribute>()
                            .FirstOrDefault();

                if (attribute == null || attribute.Analyze == Analyze.No)
                    continue;

                string name = property.Name;

                if (!String.IsNullOrWhiteSpace(attribute.Name))
                    name = attribute.Name;

                fields.Add(name);
            }

            return fields.ToArray();
        }

        public void Boost(string id)
        {
            var doc = GetDocumentById(id);
            var scoreField = doc.GetField("score");

            float score;

            if(Single.TryParse(scoreField.StringValue, out score))
                scoreField.SetValue((score + 0.1).ToString(CultureInfo.InvariantCulture));
            else
                scoreField.SetValue("1.0");

            using (var writer = GetIndexWriter())
                writer.UpdateDocument(new Term("Id", id), doc);
        }

        public void MoveItem<TType>(TType oldItem, TType newItem) where TType: IndexItem
        {
            float score;

            using (var searcher = new IndexSearcher(FSDirectory.Open(new DirectoryInfo(IndexDirectoryPath))))
            {
                var result = searcher.Search(new TermQuery(new Term("Id", oldItem.Id)), 1);
                var document = searcher.Doc(result.ScoreDocs[0].Doc);
                score = Single.Parse(document.GetField("Score").StringValue);
            }

            RemoveFromIndex(oldItem);
            Index(newItem);

            Document newDocument;

            using (var searcher = new IndexSearcher(FSDirectory.Open(new DirectoryInfo(IndexDirectoryPath))))
            {
                var result = searcher.Search(new TermQuery(new Term("Id", newItem.Id)), 1);
                newDocument = searcher.Doc(result.ScoreDocs[0].Doc);
                newDocument.GetField("Score").SetValue(score.ToString(CultureInfo.InvariantCulture));
            }

            using (var writer = GetIndexWriter())
                writer.UpdateDocument(new Term("Id", newItem.Id), newDocument);
        }
    }
}
