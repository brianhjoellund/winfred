﻿namespace Winfred.Library
{
    using System;

    public class SettingAttribute : Attribute
    {
        public SettingAttribute(string displayName, Type editorType)
        {
            DisplayName = displayName;
            EditorType = editorType;
        }

        public string DisplayName { get; set; }

        public Type EditorType { get; set; }

        public string HelpText { get; set; }
    }
}
