﻿namespace Winfred.Library
{
    using System;

    public class ValueChangedEventArgs : EventArgs
    {
        private readonly object newValue;

        public ValueChangedEventArgs(object newValue)
        {
            this.newValue = newValue;
        }

        public object NewValue
        {
            get { return newValue; }
        }
    }
}