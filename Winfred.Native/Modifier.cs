﻿namespace Winfred.Native
{
    public static class Modifier
    {
        public const uint Alt = 1;
        public const uint Ctrl = 2;
        public const uint Shift = 4;
        public const uint Windows = 8;
    }
}
