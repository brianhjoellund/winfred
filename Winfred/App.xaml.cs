﻿namespace Winfred
{
    using System;
    using System.Threading;
    using System.Windows;

    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private Thread pluginThread;

        protected override void OnStartup(StartupEventArgs e)
        {
            pluginThread = new Thread(PluginPool.LoadPlugins);
            pluginThread.Start();

            AppDomain.CurrentDomain.UnhandledException += CurrentDomainOnUnhandledException;

            base.OnStartup(e);
        }

        private void CurrentDomainOnUnhandledException(object sender, UnhandledExceptionEventArgs unhandledExceptionEventArgs)
        {
        }

        protected override void OnExit(ExitEventArgs e)
        {
            if(pluginThread != null && pluginThread.IsAlive)
                pluginThread.Abort();

            base.OnExit(e);
        }
    }
}
