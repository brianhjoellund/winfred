﻿namespace Winfred.Commands
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;
    using Winfred.Library;

    public class ExecuteSelectionCommand : ICommand
    {
        public void Execute(object parameter)
        {
            var listBox = (ListBox) parameter;
            var item = (IndexItem) listBox.SelectedItem;

            if (item == null)
                return;

            item.Execute();

            DependencyObject obj = listBox;

            while ((obj = VisualTreeHelper.GetParent(obj)) != null)
            {
                var window = obj as Window;
                
                if (window != null)
                {
                    window.Hide();
                    break;
                }
            }
        }

        public bool CanExecute(object parameter)
        {
            return parameter is ListBox;
        }

        public event EventHandler CanExecuteChanged;
    }
}