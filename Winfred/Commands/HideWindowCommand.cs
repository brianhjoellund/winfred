﻿namespace Winfred.Commands
{
    using System;
    using System.Windows;
    using System.Windows.Input;

    public class HideWindowCommand : ICommand
    {
        public void Execute(object parameter)
        {
            ((Window) parameter).Hide();
        }

        public bool CanExecute(object parameter)
        {
            return parameter is Window;
        }

        public event EventHandler CanExecuteChanged;
    }
}