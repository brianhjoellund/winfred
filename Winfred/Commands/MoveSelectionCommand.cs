﻿namespace Winfred.Commands
{
    using System;
    using System.Windows.Controls;
    using System.Windows.Input;

    public class MoveSelectionCommand : ICommand
    {
        private readonly Direction direction;

        public MoveSelectionCommand(Direction direction)
        {
            this.direction = direction;
        }

        public void Execute(object parameter)
        {
            var listBox = (ListBox) parameter;
            
            if (direction == Direction.Up)
                MoveSelectionUp(listBox);
            else
                MoveSelectionDown(listBox);

            listBox.ScrollIntoView(listBox.SelectedItem);
        }

        private void MoveSelectionUp(ListBox listBox)
        {
            if (listBox.SelectedIndex > 0)
                listBox.SelectedIndex -= 1;
        }

        private void MoveSelectionDown(ListBox listBox)
        {
            if (listBox.SelectedIndex < listBox.Items.Count - 1)
                listBox.SelectedIndex += 1;
        }

        public bool CanExecute(object parameter)
        {
            return parameter is ListBox;
        }

        public event EventHandler CanExecuteChanged;
    }
}