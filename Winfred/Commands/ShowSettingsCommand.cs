﻿namespace Winfred.Commands
{
    using System;
    using System.Windows.Input;
    using Settings;

    class ShowSettingsCommand : ICommand
    {
        public void Execute(object parameter)
        {
            var suppressHide = parameter as ISuppressHide;

            if(suppressHide != null)
                suppressHide.SuppressHide = true;

            var settingsWindow = new SettingsWindow();
            settingsWindow.ShowDialog();

            if (suppressHide != null)
                suppressHide.SuppressHide = false;
        }

        public bool CanExecute(object parameter)
        {
            return parameter is ISuppressHide;
        }

        public event EventHandler CanExecuteChanged;
    }
}