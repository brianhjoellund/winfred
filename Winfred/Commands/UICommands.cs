﻿namespace Winfred.Commands
{
    using System.Windows.Input;

    static class UICommands
    {
        private static ICommand hideWindow;
        private static ICommand moveSelectionUp;
        private static ICommand moveSelectionDown;
        private static ICommand executeSelection;
        private static ICommand showSettings;

        public static ICommand HideWindow
        {
            get
            {
                if(hideWindow == null)
                    hideWindow = new HideWindowCommand();

                return hideWindow;
            }
        }

        public static ICommand MoveSelectionUp
        {
            get
            {
                if(moveSelectionUp == null)
                    moveSelectionUp = new MoveSelectionCommand(Direction.Up);

                return moveSelectionUp;
            }
        }

        public static ICommand MoveSelectionDown
        {
            get
            {
                if(moveSelectionDown == null)
                    moveSelectionDown = new MoveSelectionCommand(Direction.Down);

                return moveSelectionDown;
            }
        }

        public static ICommand ExecuteSelection
        {
            get
            {
                if(executeSelection == null)
                    executeSelection = new ExecuteSelectionCommand();

                return executeSelection;
            }
        }

        public static ICommand ShowSettings
        {
            get
            {
                if (showSettings == null)
                    showSettings = new ShowSettingsCommand();

                return showSettings;
            }
        }
    }
}
