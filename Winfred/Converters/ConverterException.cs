﻿namespace Winfred.Converters
{
    using System;

    class ConverterException : Exception
    {
        public ConverterException(string message)
            : base(message)
        {
        }
    }
}