﻿namespace Winfred.Converters
{
    using System;
    using System.Globalization;
    using System.Windows.Data;

    class CountToHeightConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(!(value is int))
                throw new ConverterException(String.Format("Cannot convert type {0} to height", value.GetType().Name));

            int count = (int) value;

            if (count == 0)
                return 90;

            return 190;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
