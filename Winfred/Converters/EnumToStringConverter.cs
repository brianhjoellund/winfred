﻿namespace Winfred.Converters
{
    using System;
    using System.Globalization;
    using System.Windows.Data;

    public class EnumToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return null;

            if (!value.GetType().IsEnum)
                throw new Exception("This converter only converts enums");

            if(targetType != typeof(string))
                throw new Exception("Only conversion to strings are supported");

            return Enum.GetName(value.GetType(), value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return null;

            if(!(value is string))
                throw new Exception("Can only convert back from string");

            if (!targetType.IsEnum)
                throw new Exception("This converter only converts enums");

            return Enum.Parse(targetType, value as string, true);
        }
    }
}
