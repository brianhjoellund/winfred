﻿namespace Winfred.Converters
{
    using System;
    using System.Globalization;
    using System.Windows;
    using System.Windows.Data;

    class HeightToRectConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double height = (double) value;

            return new Rect(0, 0, 506, height);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
