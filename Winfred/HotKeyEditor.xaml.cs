﻿namespace Winfred
{
    using Library;
    using Native;
    using Settings;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Input;

    /// <summary>
    /// Interaction logic for HotKeyEditor.xaml
    /// </summary>
    public partial class HotKeyEditor : IInitializeControl
    {
        //private const int WM_KEYDOWN = 0x0100;
        //private const int WM_KEYUP = 0x0101;
        //private const int WM_SYSKEYDOWN = 0x0104;
        //private const int WM_SYSKEYUP = 0x0105;

        //private const int IsAltKeyBitPattern = 0x20000000;

        private class HotKeyEditorContext : DataContext
        {
            private string hotKeyGesture;
            private HotKeyMnemonic mnemonic;
            private IEnumerable<ModifierKeys> modifierKeys;
            private IEnumerable<Keys> keys;

            public string HotKeyGesture
            {
                get { return hotKeyGesture; }
                set
                {
                    if (hotKeyGesture != value)
                        return;

                    hotKeyGesture = value;
                    OnPropertyChanged("HotKeyGesture");
                }
            }

            public HotKeyMnemonic Mnemonic
            {
                get { return mnemonic; }
                set
                {
                    if(mnemonic == value)
                        return;

                    mnemonic = value;
                    OnPropertyChanged("Mnemonic");
                }
            }

            public IEnumerable<ModifierKeys> ModifierKeys
            {
                get { return modifierKeys; }
                set
                {
                    modifierKeys = value;
                    OnPropertyChanged("ModifierKeys");
                }
            }

            public IEnumerable<Keys> Keys
            {
                get { return keys; }
                set
                {
                    keys = value;
                    OnPropertyChanged("Keys");
                }
            }
        }

        public HotKeyEditor()
        {
            InitializeComponent();

            DataContext = new HotKeyEditorContext
                              {
                                  HotKeyGesture = String.Format("{0} + {1}", ModifierKeys.Alt, Keys.Space),
                                  Mnemonic = new HotKeyMnemonic
                                                 {
                                                     Key = Keys.None,
                                                     ModifierKey = ModifierKeys.None
                                                 },
                                  ModifierKeys = GetAvailableModifierKeys(),
                                  Keys = GetAvailableKeys()
                              };
        }
        
        public HotKeyMnemonic Mnemonic
        {
            get { return ((HotKeyEditorContext) DataContext).Mnemonic; }
            set
            { 
                var context = (HotKeyEditorContext) DataContext;
                context.Mnemonic = value;
                //DataContext = new HotKeyEditorContext
                //                  {
                //                      HotKeyGesture = String.Format("{0} + {1}", value.ModifierKey, value.Key),
                //                      Mnemonic = value,
                //                      Keys = context.Keys,
                //                      ModifierKeys = context.ModifierKeys
                //                  };
            }
        }

        private IEnumerable<ModifierKeys> GetAvailableModifierKeys()
        {
            return from name in Enum.GetNames(typeof (ModifierKeys))
                   where name != "None"
                   select (ModifierKeys)Enum.Parse(typeof (ModifierKeys), name);
        }

        private IEnumerable<Keys> GetAvailableKeys()
        {
            return from name in Enum.GetNames(typeof (Keys))
                   where (name.Length == 1 && name[0] >= 'A' && name[0] <= 'Z') || (name.Length == 2 && name[0] == 'D' && Char.IsNumber(name[1])) || name == "Space"
                   select (Keys) Enum.Parse(typeof (Keys), name);
        }

        /*
        private IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wparam, IntPtr lparam, ref bool handled)
        {
            switch (msg)
            {
                case WM_KEYDOWN:
                    {
                        var keys = (Keys)wparam.ToInt32();
                        Debug.WriteLine("{0}", keys);
                        return IntPtr.Zero;
                    }
                case WM_KEYUP:
                    {
                        var keys = (Keys)wparam.ToInt32();
                        Debug.WriteLine("{0}", keys);
                        return IntPtr.Zero;
                    }
                case WM_SYSKEYDOWN:
                    if ((lparam.ToInt32() & IsAltKeyBitPattern) == 1)
                    {
                        var keys = (Keys)wparam.ToInt32();
                        Debug.WriteLine("Alt+{0}", keys);
                    }
                    return IntPtr.Zero;
                case WM_SYSKEYUP:
                    if ((lparam.ToInt32() & IsAltKeyBitPattern) == 1)
                    {
                        var keys = (Keys)wparam.ToInt32();
                        Debug.WriteLine("Alt+{0}", keys);
                    }
                    return IntPtr.Zero;
            }

            return IntPtr.Zero;
        }
*/
        public void Initialize(object value)
        {
            if (value is HotKeyMnemonic)
            {
                Mnemonic = (HotKeyMnemonic) value;
            }
        }

        public event EventHandler<ValueChangedEventArgs> ValueChanged;

        private void HotKeyChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if(IsInitialized && IsLoaded && ValueChanged != null)
                ValueChanged(this, new ValueChangedEventArgs(new HotKeyMnemonic(String.Format("{0}+{1}", modifierComboBox.SelectedItem, keyComboBox.SelectedItem))));
        }
    }
}
