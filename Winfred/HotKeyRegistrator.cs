﻿namespace Winfred
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Native;
    using Settings;

    static class HotKeyRegistrator
    {
        private readonly static List<HotKey> hotKeys = new List<HotKey>();

        public static void RegisterHotKey(HotKeyMnemonic mnemonic, Action<HotKey> action)
        {
            var hotKey = new HotKey(mnemonic.ModifierKey, mnemonic.Key, IntPtr.Zero);
            hotKey.HotKeyPressed += action;
            hotKeys.Add(hotKey);
        }

        public static void UnregisterHotKey(HotKeyMnemonic mnemonic)
        {
            var hotKey = FindHotKey(mnemonic);
            
            if (hotKey != null)
            {
                hotKey.UnregisterHotKey();
                hotKeys.Remove(hotKey);
            }
        }

        private static HotKey FindHotKey(HotKeyMnemonic mnemonic)
        {
            return hotKeys.FirstOrDefault(hotKey => hotKey.KeyModifier == mnemonic.ModifierKey && hotKey.Key == mnemonic.Key);
        }

        public static void ChangeHotKey(HotKeyMnemonic oldMnemonic, HotKeyMnemonic newMnemonic)
        {
            var hotKey = FindHotKey(oldMnemonic);
            hotKey.UnregisterHotKey();
            hotKey.Key = newMnemonic.Key;
            hotKey.KeyModifier = newMnemonic.ModifierKey;
            hotKey.RegisterHotKey();
        }
    }
}
