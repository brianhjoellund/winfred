﻿namespace Winfred
{
    interface ISuppressHide
    {
        bool SuppressHide { get; set; }
    }
}