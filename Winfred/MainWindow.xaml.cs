﻿namespace Winfred
{
    using Commands;
    using Library;
    using Native;
    using Settings;
    using System;
    using System.Collections.ObjectModel;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media.Animation;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, ISuppressHide, IContext
    {
        public static readonly DependencyProperty QueryTextProperty = DependencyProperty.Register("QueryText", typeof (string), typeof (MainWindow), new PropertyMetadata(String.Empty));
        
        public ObservableCollection<IndexItem> Items { get; set; }

        public string QueryText
        {
            get { return GetValue(QueryTextProperty) as string; }
            set 
            {
                SetValue(QueryTextProperty, value);

                if(value != null)
                    searchBox.SelectionStart = value.Length;
            }
        }

        bool ISuppressHide.SuppressHide { get; set; }

        private IInputHandler CurrentInputHandler { get; set; }

        public MainWindow()
        {
            InitializeComponent();

            Items = new ObservableCollection<IndexItem>();

            Loaded += OnLoaded;

            DataContext = this;
        }

        private void OnLoaded(object sender, RoutedEventArgs args)
        {
            var settings = new GeneralSettings();
            
            HotKeyRegistrator.RegisterHotKey(settings.HotKey, ShowWindow);

            ShowWindow(null);
        }

        protected override void OnDeactivated(EventArgs e)
        {
            base.OnDeactivated(e);

            if (!((ISuppressHide)this).SuppressHide)
                Hide();

            if (!Properties.Settings.Default.ShouldSaveQueryBetweenSessions)
                searchBox.Clear();
        }

        private void ShowWindow(HotKey obj)
        {
            if (Visibility != Visibility.Visible)
            {
                if (Properties.Settings.Default.ShouldSelectPreviousQueryOnShown)
                    searchBox.SelectAll();

                Left = (SystemParameters.WorkArea.Width - ActualWidth)/2;
                Top = (SystemParameters.WorkArea.Height - 100)/2;
                Show();
                Activate();
                Keyboard.Focus(searchBox);
            }
            else
            {
                Hide();
            }
        }

        private void PerformSearch(object sender, TextChangedEventArgs e)
        {
            Items.Clear();
            
            var box = (TextBox) sender;

            CurrentInputHandler = PluginPool.GetInputHandler(box.Text);

            if (CurrentInputHandler == null)
            {
                PlayStoryboard(expand:false);
                return;
            }

            var result = CurrentInputHandler.Handle(box.Text);

            foreach (var item in result)
                Items.Add(item);

            PlayStoryboard(Items.Count);

            resultListBox.SelectedIndex = 0;
            resultListBox.ScrollIntoView(resultListBox.SelectedItem);

            if(Items.Count <= Properties.Settings.Default.MaximumItemsVisible)
                ScrollViewer.SetVerticalScrollBarVisibility(resultListBox, ScrollBarVisibility.Disabled);
            else
                ScrollViewer.SetVerticalScrollBarVisibility(resultListBox, ScrollBarVisibility.Auto);
        }

        private void PlayStoryboard(bool expand)
        {
            Storyboard storyboard = GetStoryboard(expand);
            storyboard.Begin();
        }

        private Storyboard GetStoryboard(bool expand)
        {
            if (expand)
                return Resources["expandWindow"] as Storyboard;
            
            return Resources["contractWindow"] as Storyboard;
        }

        private void PlayStoryboard(int count)
        {
            Storyboard storyboard = GetStoryboard(count > 0);

            if (count == 0)
            {
                storyboard.Begin();
            }
            else
            {
                var animation = (DoubleAnimation) storyboard.Children[0];
                animation.To = Math.Min(100 + (54.5*Properties.Settings.Default.MaximumItemsVisible), 100 + (54.5*count));
                storyboard.Begin();
            }
        }

        private void ShowSettings(object sender, MouseButtonEventArgs e)
        {
            if(UICommands.ShowSettings.CanExecute(this))
                UICommands.ShowSettings.Execute(this);
        }

        private void SearchBox_OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Tab)
            {
                if (CurrentInputHandler != null &&
                    (CurrentInputHandler.Capabilities & HandlerCapabilities.HandlesTab) ==
                    HandlerCapabilities.HandlesTab)
                {
                    var action = CurrentInputHandler.HandleTab(resultListBox.SelectedItem as IndexItem);
                    action.Execute(this);
                }
                e.Handled = true;
            }
        }
    }
}