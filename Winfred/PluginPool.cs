﻿namespace Winfred
{
    using Library;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Linq;
    using System.Reflection;

    static class PluginPool
    {
        private const string PluginDirectory = "plugins";

        private static List<IPlugin> plugins;

        public static ReadOnlyCollection<IPlugin> Plugins
        {
            get { return plugins.AsReadOnly(); }
        }

        public static void LoadPlugins()
        {
            var info = new DirectoryInfo(Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), PluginDirectory));

            if (!info.Exists)
                throw new DirectoryNotFoundException(String.Format("Could not find the plugins folder ({0})", info.FullName));

            plugins = new List<IPlugin>();

            foreach (var file in info.EnumerateFiles("*.dll"))
            {
                Assembly assembly = Assembly.LoadFile(file.FullName);

                var pluginTypes = assembly.GetExportedTypes().Where(IsPluginType);

                foreach (var pluginType in pluginTypes)
                {
                    var plugin = (IPlugin) Activator.CreateInstance(pluginType);
                    plugin.Initialize();
                    plugins.Add(plugin);
                }
            }
        }

        private static bool IsPluginType(Type type)
        {
            return typeof (IPlugin).IsAssignableFrom(type);
        }

        public static IInputHandler GetInputHandler(string input)
        {
            return plugins
                .Select(plugin => plugin.InputHandler)
                .FirstOrDefault(handler => handler.CanHandle(input));
        }
    }
}