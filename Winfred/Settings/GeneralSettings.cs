﻿namespace Winfred.Settings
{
    using System.Windows.Controls;
    using Library;
    using MainSettings = Properties.Settings;
    using LibrarySettings = Library.Properties.Settings;

    class GeneralSettings : ISettings
    {
        [Setting("Hot key", typeof(HotKeyEditor))]
        public HotKeyMnemonic HotKey
        {
            get { return new HotKeyMnemonic(MainSettings.Default.HotKeyMnemonic); }
            set
            {
                HotKeyRegistrator.ChangeHotKey(HotKey, value);

                MainSettings.Default.HotKeyMnemonic = value.ToString();
                MainSettings.Default.Save();
            }
        }

        [Setting("Maximum number of rows to return", typeof(TextBox))]
        public int MaxNumberOfRows
        {
            get { return LibrarySettings.Default.MaximumNumberOfRowsToReturn; }
            set
            {
                if (value > 0)
                {
                    LibrarySettings.Default.MaximumNumberOfRowsToReturn = value;
                    LibrarySettings.Default.Save();
                }
            }
        }

        [Setting("Number of items visible in list", typeof(TextBox))]
        public int NumberOfVisibleListItems
        {
            get { return MainSettings.Default.MaximumItemsVisible; }
            set
            {
                if (value > 0)
                {
                    MainSettings.Default.MaximumItemsVisible = value;
                    MainSettings.Default.Save();
                }
            }
        }

        [Setting("Save query", typeof(CheckBox), HelpText = "Check this if you want the query to persist between sessions")]
        public bool ShouldSaveQueryBetweenSessions
        {
            get { return MainSettings.Default.ShouldSaveQueryBetweenSessions; }
            set
            {
                MainSettings.Default.ShouldSaveQueryBetweenSessions = value;
                MainSettings.Default.Save();
            }
        }

        [Setting("Select previous query", typeof(CheckBox), HelpText = "Check this if you want the previous query to be selected when Winfred is shown")]
        public bool ShouldAutoSelectPreviousQuery
        {
            get { return MainSettings.Default.ShouldSelectPreviousQueryOnShown; }
            set
            {
                MainSettings.Default.ShouldSelectPreviousQueryOnShown = value;
                MainSettings.Default.Save();
            }
        }

    }
}
