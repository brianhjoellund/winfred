﻿namespace Winfred.Settings
{
    using System;
    using System.Text.RegularExpressions;
    using System.Windows.Input;
    using Native;

    public class HotKeyMnemonic
    {
        public HotKeyMnemonic()
        {
            ModifierKey = ModifierKeys.None;
            Key = Keys.None;
        }

        public HotKeyMnemonic(string hotkey)
        {
            ParseMnemonicFromString(hotkey);
        }

        private void ParseMnemonicFromString(string hotkey)
        {
            var m = Regex.Match(hotkey, @"^(?:(?<modifier>\w+)\+)(?<key>\w+)$");

            if (m.Success)
            {
                if (m.Groups["modifier"].Success)
                {
                    ModifierKeys modifier;
                    if (Enum.TryParse(m.Groups["modifier"].Value, out modifier))
                        ModifierKey = modifier;
                }

                Keys key;
                if (Enum.TryParse(m.Groups["key"].Value, out key))
                    Key = key;
            }

        }

        public ModifierKeys ModifierKey { get; set; }
        public Keys Key { get; set; }

        public override string ToString()
        {
            if (ModifierKey == ModifierKeys.None)
                return Key.ToString();

            return string.Format("{0}+{1}", ModifierKey, Key);
        }
    }
}