﻿namespace Winfred.Settings
{
    using Library;
    using System;
    using System.Collections.Generic;

    class SettingsNode
    {
        private IEnumerable<SettingsNode> children;

        public string Name { get; set; }
        public ISettings Settings { get; set; }

        public IEnumerable<SettingsNode> Children
        {
            get
            {
                if (children == null && HasDynamicallyLoadedChildren)
                    return LoadChildren();

                return children;
            }
            set { children = value; }
        }

        public bool HasDynamicallyLoadedChildren
        {
            get { return LoadChildren != null; }
        }

        public Func<IEnumerable<SettingsNode>> LoadChildren { get; set; }
    }
}
