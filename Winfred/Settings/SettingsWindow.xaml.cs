﻿namespace Winfred.Settings
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Reflection;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;
    using System.Windows.Media;
    using Winfred.Library;

    /// <summary>
    /// Interaction logic for Settings.xaml
    /// </summary>
    public partial class SettingsWindow : Window
    {
        private class SettingsDataContext : DataContext
        {
            private IEnumerable<SettingsNode> nodes;
            public IEnumerable<SettingsNode> Nodes
            {
                get { return nodes; }
                set
                {
                    nodes = value;
                    OnPropertyChanged("Nodes");
                }
            }
        }

        public SettingsWindow()
        {
            InitializeComponent();

            var context = new SettingsDataContext
                              {
                                  Nodes = new List<SettingsNode>
                                              {
                                                  new SettingsNode
                                                      {
                                                          Name = "General",
                                                          Settings = new GeneralSettings()
                                                      },
                                                  new SettingsNode
                                                      {
                                                          Name = "Plugins",
                                                          LoadChildren = () => PluginPool.Plugins.Select(plugin => new SettingsNode
                                                                                                                       {
                                                                                                                           Name = plugin.Name,
                                                                                                                           Settings = plugin.Settings
                                                                                                                       })
                                                      }
                                              }
                              };
            DataContext = context;
            settingsTree.Loaded += (sender, args) =>
                                       {
                                           var treeView = sender as TreeView;

                                           if(treeView == null)
                                               return;

                                           if(treeView.Items.Count == 0)
                                               return;

                                           var treeViewItem = treeView.ItemContainerGenerator.ContainerFromIndex(0) as TreeViewItem;
                                           
                                           if (treeViewItem != null) 
                                               treeViewItem.IsSelected = true;
                                       };
        }

        private void SettingsItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            var node = e.NewValue as SettingsNode;

            if (node == null || node.Settings == null)
                return;

            var settings = node.Settings;
            var properties = settings.GetType()
                                     .GetProperties()
                                     .Where(prop => prop.GetCustomAttributes(typeof(SettingAttribute), false).Length > 0);
            
            var grid = new Grid
                           {
                               HorizontalAlignment = HorizontalAlignment.Stretch,
                               VerticalAlignment = VerticalAlignment.Stretch,
                               Margin = new Thickness(10)
                           };
            grid.ColumnDefinitions.Add(new ColumnDefinition
                                           {
                                               Width = new GridLength(200)
                                           });
            grid.ColumnDefinitions.Add(new ColumnDefinition());
            
            for (int i = 0; i < properties.Count(); ++i)
                grid.RowDefinitions.Add(new RowDefinition{Height = new GridLength(0, GridUnitType.Auto) });

            grid.RowDefinitions.Add(new RowDefinition{Height = new GridLength(0, GridUnitType.Star)});

            int row = 0;
            foreach (var propertyInfo in properties)
            {
                var setting = propertyInfo.GetCustomAttributes(typeof(SettingAttribute), false).OfType<SettingAttribute>().First();
                var name = new TextBlock {Text = setting.DisplayName};
                var control = (Control)Activator.CreateInstance(setting.EditorType);
                
                name.VerticalAlignment = VerticalAlignment.Center;
                control.Margin = new Thickness(0, 3, 0, 3);

                object value = propertyInfo.GetValue(settings, null);

                var textBoxBase = control as TextBox;
                if (textBoxBase != null)
                {
                    if(value != null)
                        textBoxBase.AppendText(value.ToString());

                    PropertyInfo info = propertyInfo;
                    textBoxBase.PreviewTextInput += (o1, args1) =>
                                                        {
                                                            var box = (TextBox) o1;
                                                            var converter = TypeDescriptor.GetConverter(info.PropertyType);
                                                            args1.Handled = !converter.IsValid(box.Text.Insert(box.SelectionStart, args1.Text));
                                                        };
                    textBoxBase.TextChanged += (o, args) =>
                                                   {
                                                       var text = ((TextBox) o).Text;
                                                       var converter = TypeDescriptor.GetConverter(info.PropertyType);
                                                       
                                                       if (converter.IsValid(text))
                                                       {
                                                           info.SetValue(settings,
                                                                         converter.ConvertFromString(((TextBox) o).Text),
                                                                         null);
                                                       }
                                                       else
                                                       {
                                                           args.Handled = true;
                                                       }
                                                   };
                }
                else
                {
                    var selector = control as Selector;
                    if (selector != null)
                    {
                        if (value != null)
                        {
                            if (value is IEnumerable)
                            {
                                foreach (var val in ((IEnumerable) value))
                                    selector.Items.Add(val);

                                selector.SelectedItem = value;
                            }
                        }
                    }
                    else
                    {
                        var initializeControl = control as IInitializeControl;
                        if (initializeControl != null)
                        {
                            initializeControl.Initialize(value);
                            PropertyInfo info = propertyInfo;
                            initializeControl.ValueChanged += (o, args) => info.SetValue(settings, args.NewValue, null);
                        }
                        else
                        {
                            var checkbox = control as CheckBox;

                            if (checkbox != null)
                            {
                                checkbox.IsChecked = value is bool && (bool) value;
                                PropertyInfo info = propertyInfo;
                                checkbox.Click += (o, args) => info.SetValue(settings, checkbox.IsChecked, null);
                            }
                        }
                    }
                }

                grid.Children.Add(name);
                grid.Children.Add(control);
                Grid.SetColumn(name, 0);
                Grid.SetColumn(control, 1);
                Grid.SetRow(name, row);
                Grid.SetRow(control, row);
                row++;

                if (!String.IsNullOrWhiteSpace(setting.HelpText))
                {
                    grid.RowDefinitions.Insert(grid.RowDefinitions.Count - 2, new RowDefinition{Height = new GridLength(0, GridUnitType.Auto)});
                    var helpText = new TextBlock {Text = setting.HelpText, FontSize = FontSize-2, Foreground = Brushes.DimGray};
                    grid.Children.Add(helpText);
                    Grid.SetColumn(helpText, 0);
                    Grid.SetRow(helpText, row);
                    Grid.SetColumnSpan(helpText, 2);
                    row++;
                }
            }

            contentGrid.Children.Clear();
            contentGrid.Children.Add(grid);
        }
    }
}
