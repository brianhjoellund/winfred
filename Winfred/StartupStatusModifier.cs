﻿namespace Winfred
{
    using Microsoft.Win32;
    using System.Linq;
    using System.Reflection;

    static class StartupStatusModifier
    {
        public static void SetToRunAtWindowsStartup()
        {
            var runKey = Registry.CurrentUser.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Run", true);

            if (runKey == null)
                return;

            if (runKey.GetValueNames().Contains("Winfred"))
                return;

            var executable = Assembly.GetEntryAssembly().Location;
            runKey.SetValue("Winfred", executable, RegistryValueKind.String);
        }

        public static void UnsetToRunAtWindowsStartup()
        {
            var runKey = Registry.CurrentUser.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Run", true);

            if (runKey == null)
                return;

            runKey.DeleteValue("Winfred", false);
        }
    }
}
